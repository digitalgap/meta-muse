This layer depends on:

    URI: git://git.yoctoproject.org/poky.git
    branch: rocko
    revision: HEAD
    commit: 16e22f3

    URI: git://git.openembedded.org/meta-openembedded
    branch: rocko
    revision: HEAD
    commit: 6e3fc5b

    URI: git://git.yoctoproject.org/meta-raspberrypi 
    branch: rocko
    revision: HEAD
    commit: 510009f

    meta-muse layer maintainer: Henrik Vendelbo <henrikvendelbo@icloud.com>
