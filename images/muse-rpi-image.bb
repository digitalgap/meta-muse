DESCRIPTION = "WPE Framework image for rpi userland"
LICENSE = "MIT"

IMAGE_INSTALL = "packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL}"

# GLIBC_GENERATE_LOCALES += "en_GB.utf8"
IMAGE_LINGUAS = " "
# "en-gb.utf8"

LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "" ,d)}"

require wpe-image.inc

inherit distro_features_check

#
# Machine Selection
#
MACHINE ?= "raspberrypi0-wifi"
#MACHINE = "raspberrypi3"
#MACHINE ?= "qemuarm"
#MACHINE ?= "qemuarm64"
#MACHINE ?= "qemuppc"
MACHINE ??= "qemux86-64"

#
# Just using systemd
# all legacy sysVinit must be migrated
#
DISTRO_FEATURES_append = " systemd bluez5 bluetooth wifi"
VIRTUAL-RUNTIME_init_manager = "systemd"
DISTRO_FEATURES_BACKFILL_CONSIDERED = "sysvinit"
VIRTUAL-RUNTIME_initscripts = ""

# TODO dwc2 g_multi USB Gadget Virtual Ethernet

IMAGE_INSTALL += "linux-firmware-bcm43430 bluez5 i2c-tools python-smbus bridge-utils hostapd dhcp-server iptables wpa-supplicant nano"

IMAGE_INSTALL += "faac webrtc-audio-processing"

# IMAGE_INSTALL += "gstreamer-0.10 gstreamer-0.10-omx omxplayer"

REQUIRED_DISTRO_FEATURES = "wayland"
# DISTRO_FEATURES_remove = "wayland"

# WPE fixes https://github.com/WebPlatformForEmbedded/meta-wpe/issues/164
DISTRO_FEATURES_remove = "x11"
DISTRO_FEATURES_append += " opengl"


SPLASH_rpi = "psplash-raspberrypi"

IMAGE_FEATURES += "hwcodecs \
                   package-management \
                   ssh-server-dropbear \
                   splash \
"

IMAGE_INSTALL += "kernel-modules \
                  packagegroup-westeros \
"
# Unused WPE features
IMAGE_INSTALL_remove += "libprovision netflix"
